# The Getaway LE!

A CS-235 (SU) Project from 2020/21. Legacy build stored for archival, you can find the actively updated build here: https://gitlab.com/Patlen123/the-getaway-le

**Permissions:**

    There's a mix of old and new code here so permissions are a bit awkward and this build is just stored for archival anyway. You can find an updated build in the link above 

**Some notes on running:**

_To run without compiling_

    Set "Assets" as resources root and "Source" as sources root

_To get the font correct:_ 

    Make sure that the filepath to the program doesn't include spaces


**Credits**

_The Getaway (Legacy): https://gitlab.com/Patlen123/the-getaway_

    Joshua Oladitan
    Atif Ishaq
    Christian Sanger
    David Langmaid
    George Sanger
    Brandon Chan
    James Sam
    Daniel James Ortega

_The Getaway LE:_

    Pat Sambor
    Adilet Eshimkanov
    Ventsislav Yordanov
    Mohammed Tukur
    Aqiel Awangku Muhammad Pengiran Metusin
    Felix Ifrim
    Thomas Creswell
    Calum Atkins    
 
